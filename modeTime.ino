/*
 *    year,month,day,hour,minute,second,                                *
 *                                                                      *
 * Where                                                                *
 *    year can be two digits,                                           *
 *    month is 1-12,                                                    *
 *    day is 1-31,                                                      *
 *    hour is 0-23, and                                                 *
 *    minute and second are 0-59.                                       *
 *                                      
 */

void modeTime() {
  
  echo("Update time", "Serial...", true, false, 0);
  
  while (!confirmed()) {
    updateTime();
  }
  
  fadeLater();
  mode = LED_CHOOSE;
}

void updateTime() {

  tmElements_t tm;
  time_t currentTime;

  if (Serial.available() >= 12) {
    int y = Serial.parseInt();
    if (y >= 100 && y < 1000) {
      Serial << "Error: Year must be two digits or four digits!";
    } else {
      if (y >= 1000) {
        tm.Year = CalendarYrToTm(y);
      }
      
      tm.Year = y2kYearToTm(y);
      tm.Month = Serial.parseInt();
      tm.Day = Serial.parseInt();
      tm.Hour = Serial.parseInt();
      tm.Minute = Serial.parseInt();
      tm.Second = Serial.parseInt();
      currentTime = makeTime(tm);
      RTC.set(currentTime);
      setTime(currentTime); 
      
      Serial << F("Time updated");
      Serial << endl;
      //dump any extraneous input
      while (Serial.available() > 0) {
        Serial.read();
      }
    }
  }
}
