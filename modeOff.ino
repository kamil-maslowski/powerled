void modeOff() {

  echo("Mode off", "Darkness...", true, false, 0);
  
  analogWrite(W1, 0);
  analogWrite(W2, 0);
  analogWrite(W3, 0);
  analogWrite(R, 0);
  analogWrite(B, 0);
  
  if (switched()) {
    mode = LED_CHOOSE;
  }
}
