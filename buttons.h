static int BTN_MODE =      22;
static int BTN_PLUS =      23;
static int BTN_MINUS =     24;
static int BTN_CONFIRM =   25;

long debounceDelay = 50; 

boolean confirmed() {

  boolean retState = false;
  static int confirmBtnLastState = LOW;
  static int confirmBtnState;
  static long confirmLastDebounceTime = 0;
  
  int confirmBtnCurrent = digitalRead(BTN_CONFIRM);
  
  if (confirmBtnLastState != confirmBtnCurrent) {
    confirmLastDebounceTime = millis();
  }
  
  if ((millis() - confirmLastDebounceTime) > debounceDelay) {
    if (confirmBtnCurrent != confirmBtnState) {
      confirmBtnState = confirmBtnCurrent;
      
      if (confirmBtnCurrent == HIGH) {
        retState = true;
      }
    }
  }
  
  confirmBtnLastState = confirmBtnCurrent;
  
  return retState;
}

boolean switched() {
  
  boolean retState = false;
  static int switchBtnLastState = LOW;
  static int switchBtnState;
  static long switchLastDebounceTime = 0;
  
  int switchBtnCurrent = digitalRead(BTN_MODE);
  
  if (switchBtnLastState != switchBtnCurrent) {
    switchLastDebounceTime = millis();
  }
  
  if ((millis() - switchLastDebounceTime) > debounceDelay) {
    if (switchBtnCurrent != switchBtnState) {
      switchBtnState = switchBtnCurrent;
      
      if (switchBtnCurrent == HIGH) {
        retState = true;
      }
    }
  }
  
  switchBtnLastState = switchBtnCurrent;
  
  return retState;
}

boolean next() {
  
  boolean retState = false;
  static int nextBtnLastState = LOW;
  static int nextBtnState;
  static long nextLastDebounceTime = 0;
  
  int nextBtnCurrent = digitalRead(BTN_PLUS);
  
  if (nextBtnLastState != nextBtnCurrent) {
    nextLastDebounceTime = millis();
  }
  
  if ((millis() - nextLastDebounceTime) > debounceDelay) {
    if (nextBtnCurrent != nextBtnState) {
      nextBtnState = nextBtnCurrent;
      
      if (nextBtnCurrent == HIGH) {
        retState = true;
      }
    }
  }
  
  nextBtnLastState = nextBtnCurrent;
  
  return retState;
}

boolean prev() {
  
  boolean retState = false;
  static int prevBtnLastState = LOW;
  static int prevBtnState;
  static long prevLastDebounceTime = 0;
  
  int prevBtnCurrent = digitalRead(BTN_MINUS);
  
  if (prevBtnLastState != prevBtnCurrent) {
    prevLastDebounceTime = millis();
  }
  
  if ((millis() - prevLastDebounceTime) > debounceDelay) {
    if (prevBtnCurrent != prevBtnState) {
      prevBtnState = prevBtnCurrent;
      
      if (prevBtnCurrent == HIGH) {
        retState = true;
      }
    }
  }
  
  prevBtnLastState = prevBtnCurrent;
  
  return retState;
}
