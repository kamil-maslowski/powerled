long lastInfoChange = 0;
long lastClockUpdate = 0;
long ledLastUpdateTime = 0;
Info info;

void modeAuto(time_t currentTime) {
  
  if (millis() - lastInfoChange > 5000) {
    String msg = "";
    switch (info) {
      case TEMP:
      default:
        msg = msg + "W:" + ledIntensityW + "R:" + ledIntensityR + "B:" + ledIntensityB;
        echo("", msg, false, false, 0);
        info = INTENSITY;
        break;
      case INTENSITY:
        msg = msg + "Temp: " + (RTC.temperature() / 4) + "*C";
        echo("", msg, true, false, 0);
        info = TEMP;
        break;
    }
    lastInfoChange = millis();
  }
  
  long clockUpdate = toSeconds(hour(currentTime), minute(currentTime), second(currentTime));
  if (lastClockUpdate == 0 || clockUpdate != lastClockUpdate) {
    int h = hour(currentTime);
    int m = minute(currentTime);
    int s = second(currentTime);
    String clockEcho = toLiquidTime(h, m, s);
    echo(clockEcho, "", true, false, 0);
    lastClockUpdate = clockUpdate;
  }
}

void updateLights(time_t currentTime) {
	
  long now = toSeconds(hour(currentTime), minute(currentTime), second(currentTime));

  if (now != ledLastUpdateTime) {
    String msg = "";
    
    for (int channel = 0; channel < LED_CHANNELS; channel++) {
      int currentPin = Channels[channel].GetPin();
      int currentLightIntensity = Channels[channel].GetLightIntensityInt(now);
      
      analogWrite(currentPin, currentLightIntensity);
      
      if (currentPin == W1) {
        ledIntensityW = currentLightIntensity;
      } else if (currentPin == R) {
        ledIntensityR = currentLightIntensity;
      } else if (currentPin == B) {
        ledIntensityB = currentLightIntensity;
      }
    }
    
  }
  
  ledLastUpdateTime = now;
}
