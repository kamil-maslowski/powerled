ManualMode manualToConfirm;

void modeManual() {

  switch (manualToConfirm) {
    case ALL_MAX:
      echo("Manual - select", "All max", true, false, 0);
      break;
    case ALL_MANUAL:
      echo("Manual - select", "All manual", true, false, 0);
      break;
    case W_MAX:
      echo("Manual - select", "White max", true, false, 0);
      break;
    case R_MAX:
      echo("Manual - select", "Red max", true, false, 0);
      break;
    case B_MAX:
      echo("Manual - select", "Blue max", true, false, 0);
      break;
    case W_MANUAL:
      echo("Manual - select", "White manual", true, false, 0);
      break;
    case R_MANUAL:
      echo("Manual - select", "Red manual", true, false, 0);
      break;
    case B_MANUAL:
      echo("Manual - select", "Blue manual", true, false, 0);
      break;
    case MANUAL_NONE:
      break;
    default:
      manualToConfirm = ALL_MAX;
      break;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL_DRIVE;
  }
  
  shouldDisplayAnotherManualMode();
}

ManualMode getNextManualMode() {
  if (manualToConfirm == ALL_MAX) return ALL_MANUAL;
  if (manualToConfirm == ALL_MANUAL) return W_MAX;
  if (manualToConfirm == W_MAX) return R_MAX;
  if (manualToConfirm == R_MAX) return B_MAX;
  if (manualToConfirm == B_MAX) return W_MANUAL;
  if (manualToConfirm == W_MANUAL) return R_MANUAL;
  if (manualToConfirm == R_MANUAL) return B_MANUAL;
  if (manualToConfirm == B_MANUAL) return ALL_MAX;
}

ManualMode getPrevManualMode() {
  if (manualToConfirm == ALL_MAX) return B_MANUAL;
  if (manualToConfirm == ALL_MANUAL) return ALL_MAX;
  if (manualToConfirm == W_MAX) return ALL_MANUAL;
  if (manualToConfirm == R_MAX) return W_MAX;
  if (manualToConfirm == B_MAX) return R_MAX;
  if (manualToConfirm == W_MANUAL) return B_MAX;
  if (manualToConfirm == R_MANUAL) return W_MANUAL;
  if (manualToConfirm == B_MANUAL) return R_MANUAL;
}

void shouldDisplayAnotherManualMode() {

  if (next()) {
    fadeLater();
    manualToConfirm = getNextManualMode();
  }
  
  if (prev()) {
    fadeLater();
    manualToConfirm = getPrevManualMode();
  }
}

