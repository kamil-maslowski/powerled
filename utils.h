enum Info {
  INTENSITY, TEMP, OTHER
};

enum Mode {
  LED_AUTO, LED_MANUAL, LED_MANUAL_DRIVE, LED_OFF, LED_CHOOSE, LED_TIME
};

enum ManualMode {
  ALL_MAX, ALL_MANUAL, W_MAX, R_MAX, B_MAX, W_MANUAL, R_MANUAL, B_MANUAL, MANUAL_NONE
};

String withLeading(int digit) {
  
  if (digit == 0) {
    return "00";
  }
  
  if (digit < 10) {
    String out = "0";
    out.concat(digit);
    return out;
  }
  
  return String(digit);
}

String toLiquidTime(int h, int m, int s) {

  String liquid = withLeading(h);
  liquid.concat(":");
  liquid.concat(withLeading(m));
  liquid.concat(":");
  liquid.concat(withLeading(s));
  
  return liquid;
}
