#include <LiquidCrystal.h>
#include <Streaming.h>
#include <ChannelManager.h>
#include <Time.h>
#include <Wire.h>
#include <DS3232RTC.h>
#include "debug.h"
#include "utils.h"
#include "buttons.h"
#include "liquid.h"

int ledIntensityW = 0;
int ledIntensityR = 0;
int ledIntensityB = 0;

/*
  LED pins
 */
static int W1 = 2;
static int W2 = 3;
static int W3 = 4;
static int R = 5;
static int B = 6;

const int LED_CHANNELS = 5;
const int LED_MAX_POINTS = 8;

Channel Channels[LED_CHANNELS];
Point Points[LED_CHANNELS][LED_MAX_POINTS];

const int LED_CHANNELS_PINS[] = {W1, W2, W3, R, B};
const int LED_WHITE_CHANNEL_POINTS[LED_MAX_POINTS][3] = {
  {14, 0, 0},
  {14, 30, 180},
  {16, 30, 200},
  {17, 0, 250},
  {21, 0, 250},
  {21, 30, 200},
  {23, 0, 170},
  {23, 30, 0}
};

const int LED_RED_CHANNEL_POINTS[LED_MAX_POINTS][3] = {
  {14, 0, 0},
  {14, 30, 180},
  {16, 30, 200},
  {17, 0, 250},
  {21, 0, 250},
  {21, 30, 200},
  {23, 0, 170},
  {23, 50, 0}
};

const int LED_BLUE_CHANNEL_POINTS[LED_MAX_POINTS][3] = {
  {14, 0, 0},
  {14, 30, 100},
  {16, 30, 200},
  {17, 0, 250},
  {21, 0, 250},
  {21, 30, 200},
  {23, 0, 170},
  {23, 50, 0}
};

/*
  Fan pins
*/
static int FAN_ENABLE = 27;
static int FAN_INPUT_1 = 29;
static int FAN_INPUT_2 = 31;

/*
  stuff
 */
static int LED_PWM_OFF = 0;
static int LED_PWM_MAX = 250;
static int LED_PWM_STEP = 1;

Mode mode;

void setup()  {

  mode = LED_AUTO;
  
  setupLcd();
  welcome();
  setupDebug();
  setupTime();
  setupPins();

  initStartupValues();
  setupLed();
} 

void setupLed() {
  
  for (int i = 0; i < LED_CHANNELS; i++) {
    Channels[i] = Channel(LED_CHANNELS_PINS[i], LED_MAX_POINTS, fademode_linear, Points[i]);
    
    if (LED_CHANNELS_PINS[i] == W1 || LED_CHANNELS_PINS[i] == W2 || LED_CHANNELS_PINS[i] == W3) {
      for (int j = 0; j < LED_MAX_POINTS; j++) {
        Channels[i].AddPoint(LED_WHITE_CHANNEL_POINTS[j][0], LED_WHITE_CHANNEL_POINTS[j][1], LED_WHITE_CHANNEL_POINTS[j][2]);
      }
    }
    
    if (LED_CHANNELS_PINS[i] == B) {
      for (int j = 0; j < LED_MAX_POINTS; j++) {
        Channels[i].AddPoint(LED_BLUE_CHANNEL_POINTS[j][0], LED_BLUE_CHANNEL_POINTS[j][1], LED_BLUE_CHANNEL_POINTS[j][2]);
      }
    }
    
    if (LED_CHANNELS_PINS[i] == R) {
      for (int j = 0; j < LED_MAX_POINTS; j++) {
        Channels[i].AddPoint(LED_RED_CHANNEL_POINTS[j][0], LED_RED_CHANNEL_POINTS[j][1], LED_RED_CHANNEL_POINTS[j][2]);
      }
    }
  }
}

void welcome() {
  echo("Welcome", "Azure LED v 1.0", true, true, 2000);
}

void setupDebug() {

  Serial.begin(115200);
}

void setupTime() {

  setSyncProvider(RTC.get);
  setSyncInterval(600);
}

void setupLcd() {

  lcd.begin(LCD_COLS, LCD_ROWS);\
}

void setupPins() {
  
  pinMode(W1, OUTPUT);
  pinMode(W2, OUTPUT);
  pinMode(W3, OUTPUT);
  pinMode(R, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(LCD_PIN_BACKLIGHT, OUTPUT);
  
  pinMode(BTN_MODE, INPUT);
  pinMode(BTN_PLUS, INPUT);
  pinMode(BTN_MINUS, INPUT);
  pinMode(BTN_CONFIRM, INPUT);
  
  pinMode(FAN_ENABLE, OUTPUT);
  pinMode(FAN_INPUT_1, OUTPUT);
  pinMode(FAN_INPUT_2, OUTPUT);
}

void initStartupValues() {
  
  analogWrite(W1, LED_PWM_OFF);
  analogWrite(W2, LED_PWM_OFF);
  analogWrite(W3, LED_PWM_OFF);
  analogWrite(R, LED_PWM_OFF);
  analogWrite(B, LED_PWM_OFF); 
  
  digitalWrite(FAN_ENABLE, HIGH);
  digitalWrite(FAN_INPUT_1, LOW);
  digitalWrite(FAN_INPUT_2, HIGH);
  
  fadeLater();
}

long toSeconds(int hours, int minutes, int seconds) {
  
  return ((long)hours * 60 * 60) + (minutes * 60) + seconds ;
}

void shouldSwitchOn() {
  
  static int turnOnBtnLastState = LOW;
  static int turnOnBtnState;
  static long turnOnLastDebounceTime = 0;
  
  int turnOnBtnCurrent = digitalRead(BTN_MODE);
  
  if (turnOnBtnLastState != turnOnBtnCurrent) {
    turnOnLastDebounceTime = millis();
  }
  
  if ((millis() - turnOnLastDebounceTime) > debounceDelay) {
    if (turnOnBtnCurrent != turnOnBtnState) {
      turnOnBtnState = turnOnBtnCurrent;
      
      if (turnOnBtnCurrent == HIGH) {
        fadeIn();
      }
    }
  }
  
  turnOnBtnLastState = turnOnBtnCurrent;
}

void shouldSwitchMode() {

  static int switchBtnLastState = LOW;
  static int switchBtnState;
  static long switchLastDebounceTime = 0;
  
  int switchBtnCurrent = digitalRead(BTN_MODE);
  
  if (switchBtnLastState != switchBtnCurrent) {
    switchLastDebounceTime = millis();
  }
  
  if ((millis() - switchLastDebounceTime) > debounceDelay) {
    if (switchBtnCurrent != switchBtnState) {
      switchBtnState = switchBtnCurrent;
      
      if (switchBtnCurrent == HIGH) {
        mode = LED_CHOOSE;
      }
    }
  }
  
  switchBtnLastState = switchBtnCurrent;
}

void loop()  { 
  time_t currentTime = RTC.get();
  
  if (lcdState == ON && (millis() - fadeInTime) > fadeOutAfter) {
    fadeOut();
  }
  
  if (lcdState == OFF) {
    shouldSwitchOn();
  }
  
  if (lcdState == ON) {
    shouldSwitchMode();
  }
  
  switch (mode) {
    case LED_CHOOSE:
      modeChoose();
      break;
    case LED_AUTO:
      updateLights(currentTime);
      modeAuto(currentTime);
      break;
    case LED_MANUAL:
      modeManual();
      break;
    case LED_MANUAL_DRIVE:
      modeManualDrive();
      break;
    case LED_OFF:
      modeOff();
      break;
    case LED_TIME:
      modeTime();
      break;
    default:
      updateLights(currentTime);
      modeAuto(currentTime);
      break;
  }
}

