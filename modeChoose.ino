Mode chooseToConfirm;

void shouldDisplayAnotherMode() {

  if (next()) {
    fadeLater();
    chooseToConfirm = getNextMode();
  }
  
  if (prev()) {
    fadeLater();
    chooseToConfirm = getPrevMode();
  }
}

Mode getNextMode() {
  if (chooseToConfirm == LED_AUTO) return LED_MANUAL;
  if (chooseToConfirm == LED_MANUAL) return LED_OFF;
  if (chooseToConfirm == LED_OFF) return LED_TIME; 
  if (chooseToConfirm == LED_TIME) return LED_AUTO; 
}

Mode getPrevMode() {
  if (chooseToConfirm == LED_AUTO) return LED_TIME;
  if (chooseToConfirm == LED_TIME) return LED_OFF;
  if (chooseToConfirm == LED_OFF) return LED_MANUAL;
  if (chooseToConfirm == LED_MANUAL) return LED_AUTO;
}

void modeChoose() {
  
  switch (chooseToConfirm) {
    case LED_AUTO:
      echo("Mode", "Auto", true, false, 0);
      break;
    case LED_MANUAL:
      echo("Mode", "Manual", true, false, 0);
      break;
    case LED_OFF:
      echo("Mode", "Off", true, false, 0);
      break;
    case LED_TIME:
      echo("Mode", "Update time", true, false, 0);
      break;
    case LED_CHOOSE:
    default:
      chooseToConfirm = LED_AUTO;
      echo("Mode", "Auto", true, false, 0);
      break;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = chooseToConfirm;
  }
  shouldDisplayAnotherMode();
  
}
