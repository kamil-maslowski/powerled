void modeManualDrive() {

  switch (manualToConfirm) {
    case ALL_MAX:
      allMaxMode();
      break;
    case ALL_MANUAL:
      allManualMode();
      break;
    case W_MAX:
      whiteMaxMode();
      break;
    case R_MAX:
      redMaxMode();
      break;
    case B_MAX:
      blueMaxMode();
      break;
    case W_MANUAL:
      whiteManualMode();
      break;
    case R_MANUAL:
      redManualMode();
      break;
    case B_MANUAL:
      blueManualMode();
      break;
    default:
      break;
  }
}


void allMaxMode() {
  
  echo("Manual mode", "All max", true, false, 0);
  
  analogWrite(W1, 255);
  analogWrite(W2, 255);
  analogWrite(W3, 255);
  analogWrite(R, 255);
  analogWrite(B, 255);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}

void allManualMode() {
  
  static int manIntensity = 0;
  String msg = String("All manual: ");
  msg.concat(manIntensity);
  echo("Manual mode", msg, true, false, 0);
  
  if (next() && manIntensity < 255) {
    fadeLater();
    manIntensity++;
  }
  
  if (prev() && manIntensity > 0) {
    fadeLater();
    manIntensity--;
  }
  
  analogWrite(W1, manIntensity);
  analogWrite(W2, manIntensity);
  analogWrite(W3, manIntensity);
  analogWrite(R, manIntensity);
  analogWrite(B, manIntensity);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}

void whiteMaxMode() {

  echo("Manual mode", "White max", true, false, 0);
  
  analogWrite(W1, 255);
  analogWrite(W2, 255);
  analogWrite(W3, 255);
  analogWrite(R, 0);
  analogWrite(B, 0);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}

void redMaxMode() {

  echo("Manual mode", "Red max", true, false, 0);
  
  analogWrite(W1, 0);
  analogWrite(W2, 0);
  analogWrite(W3, 0);
  analogWrite(R, 255);
  analogWrite(B, 0);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}

void blueMaxMode() {

  echo("Manual mode", "Blue max", true, false, 0);
  
  analogWrite(W1, 0);
  analogWrite(W2, 0);
  analogWrite(W3, 0);
  analogWrite(R, 0);
  analogWrite(B, 255);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}

void whiteManualMode() {

  static int whiteIntensity = 0;
  String msg = String("White manual: ");
  msg.concat(whiteIntensity);
  echo("Manual mode", msg, true, false, 0);
  
  if (next() && whiteIntensity < 255) {
    fadeLater();
    whiteIntensity++;
  }
  
  if (prev() && whiteIntensity > 0) {
    fadeLater();
    whiteIntensity--;
  }
  
  analogWrite(W1, whiteIntensity);
  analogWrite(W2, whiteIntensity);
  analogWrite(W3, whiteIntensity);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}

void redManualMode() {

  static int redIntensity = 0;
  String msg = String("Red manual: ");
  msg.concat(redIntensity);
  echo("Manual mode", msg,  true, false, 0);
  
  if (next() && redIntensity < 255) {
    fadeLater();
    redIntensity++;
  }
  
  if (prev() && redIntensity > 0) {
    fadeLater();
    redIntensity--;
  }
  
  analogWrite(R, redIntensity);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}

void blueManualMode() {

  static int blueIntensity = 0;
  String msg = String("Blue manual: ");
  msg.concat(blueIntensity);
  echo("Manual mode", msg, true, false, 0);
  
  if (next() && blueIntensity < 255) {
    fadeLater();
    blueIntensity++;
  }
  
  if (prev() && blueIntensity > 0) {
    fadeLater();
    blueIntensity--;
  }
  
  analogWrite(B, blueIntensity);
  
  if (switched()) {
    fadeLater();
    mode = LED_CHOOSE;
  }
  
  if (confirmed()) {
    fadeLater();
    mode = LED_MANUAL;
  }
}
