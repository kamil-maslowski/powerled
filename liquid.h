#define LCD_PIN_BACKLIGHT    8
#define LCD_COLS            16
#define LCD_ROWS             2
#define LCD_PIN_RS          35
#define LCD_PIN_E           37
#define LCD_PIN_D4          39
#define LCD_PIN_D5          41
#define LCD_PIN_D6          43
#define LCD_PIN_D7          45

enum LCDState {
  ON, OFF, FADING
};

LiquidCrystal lcd(LCD_PIN_RS, LCD_PIN_E, LCD_PIN_D4, LCD_PIN_D5, LCD_PIN_D6, LCD_PIN_D7);


LCDState lcdState = OFF;

String echoFirst = "";
String echoSecond = "";

long fadeInTime = 0;
long fadeOutAfter = 15000;

void fadeLater() {
  fadeInTime = millis();
}

void fadeIn() {
  
  if (lcdState != OFF) {
    return;
  }
  
  lcdState = FADING;
  
  for (int i = 0; i <= 255; i++) {
    analogWrite(LCD_PIN_BACKLIGHT, i);
    delay(5);
  }
  
  fadeInTime = millis();
  lcdState = ON;
}

void fadeOut() {
  
  if (lcdState != ON) {
    return;
  }
  
  lcdState = FADING;
  
  for (int i = 255; i >= 0; i--) {
    analogWrite(LCD_PIN_BACKLIGHT, i);
    delay(5);
  }
  
  fadeInTime = 0;
  lcdState = OFF;
}

void echo(String first, String second, boolean center, boolean fade, int wait) {
  
  if (first == echoFirst && second == echoSecond) {
    return;
  }

  if (fade) {
    fadeIn();
  }

  int spacesFirst = 0;
  int spacesSecond = 0;
  boolean hasFirst = first.length() > 0;
  boolean hasSecond = second.length() > 0;

  if (hasFirst && center) {
    int diff = ((LCD_COLS - first.length()) > 0 ? (LCD_COLS - first.length()) : 0);
    spacesFirst = diff / 2;
  } else if (hasFirst) {
    spacesFirst = LCD_COLS - first.length();
  }

  if (hasSecond && center) {
    int diff = ((LCD_COLS - second.length()) > 0 ? (LCD_COLS - second.length()) : 0);
    spacesSecond = diff / 2;
  } else if (hasSecond) {
    spacesSecond = LCD_COLS - second.length();
  }

  lcd.setCursor(0, 0);
  if (center) {
    for (int i = 0; i < spacesFirst; i++) {
      lcd.print(" ");
    }
  }
  
  if (hasFirst) {
//    d("echo() print first line");
    lcd.print(first);
    echoFirst = first;
  }
  for (int i = 0; i < spacesFirst; i++) {
    lcd.print(" ");
  }

  lcd.setCursor(0, 1);
  if (center) {
    for (int i = 0; i < spacesSecond; i++) {
      lcd.print(" ");
    }
  }
  
  if (hasSecond) {
    lcd.print(second);
    echoSecond = second;
  }
  for (int i = 0; i < spacesSecond; i++) {
    lcd.print(" ");
  }

  delay(wait);
}
